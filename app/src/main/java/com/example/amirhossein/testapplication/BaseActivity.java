package com.example.amirhossein.testapplication;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.github.nisrulz.sensey.Sensey;
import com.github.nisrulz.sensey.ShakeDetector;

import java.io.ByteArrayOutputStream;

public class BaseActivity extends AppCompatActivity {
    public Context mContext = this ;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Sensey.getInstance().init(this);
        ShakeDetector.ShakeListener shakeListener = new ShakeDetector.ShakeListener() {
            @Override
            public void onShakeDetected() {
                Toast.makeText(mContext, "onShakeDetected", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onShakeStopped() {
                Toast.makeText(mContext, "onShakeStopped", Toast.LENGTH_SHORT).show();
                openFeedbackActivity(takeScreenshot()) ;
            }
        };
        Sensey.getInstance().startShakeDetection(shakeListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Sensey.getInstance().stop();
    }

    void openFeedbackActivity(Bitmap bitmap){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();

        Intent in1 = new Intent(this, ShowScreenShotActivity.class);
        in1.putExtra("image",byteArray);
        startActivity(in1);
    }

    public Bitmap takeScreenshot() {
        View rootView = findViewById(android.R.id.content).getRootView();
        rootView.setDrawingCacheEnabled(true);
        return rootView.getDrawingCache();
    }
}
