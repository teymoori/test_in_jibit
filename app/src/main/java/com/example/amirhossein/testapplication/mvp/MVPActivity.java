package com.example.amirhossein.testapplication.mvp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.amirhossein.testapplication.R;
import com.example.amirhossein.testapplication.retrofit.API;
import com.example.amirhossein.testapplication.retrofit.APIInterface;
import com.example.amirhossein.testapplication.retrofit.POJOModel;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MVPActivity extends AppCompatActivity implements MVPContract.View {

     MVPPresenter presenter ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mvp);
        presenter = new MVPPresenter() ;
        presenter.attachView(this);

        try{
            Response<POJOModel> response = API.getClient().create(APIInterface.class).getIP().execute();
        }catch (Exception e){
        }
        API.getClient().create(APIInterface.class).getIP().enqueue(new Callback<POJOModel>() {
            @Override
            public void onResponse(Call<POJOModel> call, Response<POJOModel> response) {
                Toast.makeText(MVPActivity.this, response.body().getCity(), Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onFailure(Call<POJOModel> call, Throwable t) {
                Toast.makeText(MVPActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void showName(String name) {
        Toast.makeText(this, name, Toast.LENGTH_SHORT).show();
    }
}
