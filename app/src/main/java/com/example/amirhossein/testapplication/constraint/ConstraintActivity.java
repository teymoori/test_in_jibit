package com.example.amirhossein.testapplication.constraint;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.amirhossein.testapplication.R;

public class ConstraintActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_constraint);
    }
}
