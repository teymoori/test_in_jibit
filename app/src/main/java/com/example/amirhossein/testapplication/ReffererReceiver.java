package com.example.amirhossein.testapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class ReffererReceiver extends BroadcastReceiver{
    private String referrer = "";


    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("com.android.vending.INSTALL_REFERRER")) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                referrer = extras.getString("referrer");
            }
            Toast.makeText(context, "referrer : " + referrer, Toast.LENGTH_SHORT).show();
        }
    }
}
