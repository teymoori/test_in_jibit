package com.example.amirhossein.testapplication.mvp;

public interface MVPContract {

    interface View{
        void showName(String name) ;
    }
    interface Presenter{
        void attachView( View view) ;
        void onName(String name) ;
    }
    interface Model{
        void attachPresenter(Presenter presenter) ;
        void getName() ;
    }
}
