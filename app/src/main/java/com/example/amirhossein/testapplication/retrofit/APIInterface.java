package com.example.amirhossein.testapplication.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIInterface {
    @GET("json")
    Call<POJOModel> getIP();
}
