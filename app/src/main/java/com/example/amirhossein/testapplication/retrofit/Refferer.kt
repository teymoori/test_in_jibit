package com.example.amirhossein.testapplication.retrofit


import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.android.installreferrer.api.InstallReferrerClient
import com.android.installreferrer.api.InstallReferrerClient.InstallReferrerResponse
import com.android.installreferrer.api.InstallReferrerStateListener
import com.example.amirhossein.testapplication.R

class Refferer : AppCompatActivity() {
    val mContext : Context = this
    private lateinit var referrerClient: InstallReferrerClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_get_installers)


        findViewById<Button>(R.id.get).setOnClickListener(  View.OnClickListener{
            show()
        })

    }


    fun show(){

        referrerClient = InstallReferrerClient.newBuilder(this).build()
        referrerClient.startConnection(object : InstallReferrerStateListener {
            override fun onInstallReferrerSetupFinished(responseCode: Int) {
                when (responseCode) {
                    InstallReferrerResponse.OK -> {
                        val response = referrerClient.installReferrer
                        Toast.makeText(mContext, "referrer-- : " + response.installReferrer, Toast.LENGTH_SHORT).show()
                        Log.d("testtest 1", response.installReferrer)
                        Log.d("testtest 2", response.referrerClickTimestampSeconds.toString())
                        Log.d("testtest 3", response.installBeginTimestampSeconds.toString())
                        referrerClient.endConnection()
                    }
                    InstallReferrerResponse.FEATURE_NOT_SUPPORTED -> {

                    }
                    InstallReferrerResponse.SERVICE_UNAVAILABLE -> {
                    }
                }
            }

            override fun onInstallReferrerServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
            }
        })
    }

}