package com.example.amirhossein.testapplication.musicplayer.pojo;

import com.orm.SugarRecord;

public class MusicPojo extends SugarRecord<MusicPojo> {

    public String title;
    public String album;
    public String singer;
    public int time;
    public String cover;
    public String url ;


    public MusicPojo() {
        super();
    }

    private MusicPojo(Builder builder) {
        super();
        setTitle(builder.title);
        setAlbum(builder.album);
        setSinger(builder.singer);
        setTime(builder.time);
        setCover(builder.cover);
        setUrl(builder.url);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public static final class Builder {
        private String title;
        private String album;
        private String singer;
        private int time;
        private String cover;
        private String url;

        private Builder() {
        }

        public Builder title(String val) {
            title = val;
            return this;
        }

        public Builder album(String val) {
            album = val;
            return this;
        }

        public Builder singer(String val) {
            singer = val;
            return this;
        }

        public Builder time(int val) {
            time = val;
            return this;
        }

        public Builder cover(String val) {
            cover = val;
            return this;
        }

        public Builder url(String val) {
            url = val;
            return this;
        }

        public MusicPojo build() {
            return new MusicPojo(this);
        }
    }

    @Override
    public String toString() {
        return "MusicPojo{" +
                "title='" + title + '\'' +
                ", album='" + album + '\'' +
                ", singer='" + singer + '\'' +
                ", time=" + time +
                ", cover='" + cover + '\'' +
                ", url='" + url + '\'' +
                ", id=" + id +
                '}';
    }
}
