package com.example.amirhossein.testapplication.musicplayer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.amirhossein.testapplication.R;
import com.example.amirhossein.testapplication.musicplayer.pojo.MusicPojo;

import java.util.List;

public class MusicsListAdapter extends BaseAdapter {
    Context mContext ;
    List<MusicPojo> musics ;

    public MusicsListAdapter(Context mContext, List<MusicPojo> musics) {
        this.mContext = mContext;
        this.musics = musics;
    }

    @Override
    public int getCount() {
        return musics.size();
    }

    @Override
    public Object getItem(int i) {
        return musics.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.music_item , viewGroup , false) ;
        TextView title = v.findViewById(R.id.title);
        title.setText(musics.get(i).getTitle());
        return v;
    }
}
