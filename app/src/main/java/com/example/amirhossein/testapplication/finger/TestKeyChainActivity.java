package com.example.amirhossein.testapplication.finger;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.amirhossein.testapplication.BaseActivity;
import com.example.amirhossein.testapplication.R;

public class TestKeyChainActivity extends BaseActivity implements View.OnClickListener {
    EditText alias, word, decryptAlias;
    Button encrypt, decrypt;
    private KeyChainHandler keyChainHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_key_chain);
        bind();
    }

    void bind() {
        alias = findViewById(R.id.alias);
        word = findViewById(R.id.word);
        decryptAlias = findViewById(R.id.decryptAlias);
        encrypt = findViewById(R.id.encrypt);
        decrypt = findViewById(R.id.decrypt);
        encrypt.setOnClickListener(this);
        decrypt.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.encrypt)
            encrypt();
        else if (view.getId() == R.id.decrypt)
            decrypt();
    }

    void encrypt() {
        keyChainHandler = new KeyChainHandler(mContext, alias.getText().toString());
        keyChainHandler.encryptString(alias.getText().toString(), word.getText().toString());
    }

    void decrypt() {
        if (keyChainHandler == null)
            keyChainHandler = new KeyChainHandler(mContext, decryptAlias.getText().toString());

        Toast.makeText(mContext, keyChainHandler.decryptString(decryptAlias.getText().toString()), Toast.LENGTH_SHORT).show();
    }
}
