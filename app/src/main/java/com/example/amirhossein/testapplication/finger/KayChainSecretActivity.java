package com.example.amirhossein.testapplication.finger;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.preference.PreferenceManager;
import android.security.KeyPairGeneratorSpec;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.amirhossein.testapplication.BaseActivity;
import com.example.amirhossein.testapplication.R;

import android.util.Base64;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.security.auth.x500.X500Principal;


public class KayChainSecretActivity extends BaseActivity {

    static final String TAG = "keychain_";
    static final String CIPHER_TYPE = "RSA/ECB/PKCS1Padding";
    static final String CIPHER_PROVIDER = "AndroidOpenSSL";

    String encrypted = "";


    EditText aliasText;
    EditText startText, decryptedText, encryptedText;
    List<String> keyAliases;
    ListView listView;
    //  KeyRecyclerAdapter listAdapter;

    KeyStore keyStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
            keyStore.load(null);
            Log.d(TAG, "loaded ");
        } catch (Exception e) {
            Log.d(TAG, "Exception " + e);
        }

        createNewKeys("amir");
        encryptString("amir", "AmirHosssein Teymoori 123");

//        decryptString("amir") ;
        //       refreshKeys();


        decryptString("amir", getShared("main_pass"));
        setContentView(R.layout.activity_main);


    }

    private void refreshKeys() {
        keyAliases = new ArrayList<>();
        try {
            Enumeration<String> aliases = keyStore.aliases();
            Log.d(TAG, "aliases size " + aliases);
            while (aliases.hasMoreElements()) {
                keyAliases.add(aliases.nextElement());
                Log.d(TAG, "while");
            }
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }


        for (String str :
                keyAliases) {
            Log.d(TAG, "refreshKeys: " + str);
        }
    }

    public void createNewKeys(String alias) {

        try {
            // Create new key if needed
            if (!keyStore.containsAlias(alias)) {
                Calendar start = Calendar.getInstance();
                Calendar end = Calendar.getInstance();
                end.add(Calendar.YEAR, 1);
                KeyPairGeneratorSpec spec = new KeyPairGeneratorSpec.Builder(this)
                        .setAlias(alias)
                        .setSubject(new X500Principal("CN=WallertJibit, O=Jibit"))
                        .setSerialNumber(BigInteger.ONE)
                        .setStartDate(start.getTime())
                        .setEndDate(end.getTime())
                        .build();
                KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");
                generator.initialize(spec);

                KeyPair keyPair = generator.generateKeyPair();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Exception " + e.getMessage() + " occured", Toast.LENGTH_LONG).show();
            Log.e(TAG, Log.getStackTraceString(e));
        }
        refreshKeys();
    }

    public void deleteKey(final String alias) {
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle("Delete Key")
                .setMessage("Do you want to delete the key \"" + alias + "\" from the keystore?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            keyStore.deleteEntry(alias);
                            refreshKeys();
                        } catch (KeyStoreException e) {
                            Toast.makeText(mContext,
                                    "Exception " + e.getMessage() + " occured",
                                    Toast.LENGTH_LONG).show();
                            Log.e(TAG, Log.getStackTraceString(e));
                        }
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        alertDialog.show();
    }

    public void encryptString(String alias, String initialText) {
        try {
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(alias, null);
            RSAPublicKey publicKey = (RSAPublicKey) privateKeyEntry.getCertificate().getPublicKey();


            if (initialText.isEmpty()) {
                Toast.makeText(this, "Enter text in the 'Initial Text' widget", Toast.LENGTH_LONG).show();
                return;
            }

            Cipher inCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding", "AndroidOpenSSL");
            inCipher.init(Cipher.ENCRYPT_MODE, publicKey);

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            CipherOutputStream cipherOutputStream = new CipherOutputStream(
                    outputStream, inCipher);
            cipherOutputStream.write(initialText.getBytes("UTF-8"));
            cipherOutputStream.close();

            byte[] vals = outputStream.toByteArray();
            //  encryptedText.setText(Base64.encodeToString(vals, Base64.DEFAULT));

            encrypted = Base64.encodeToString(vals, Base64.DEFAULT);
            setShared("main_pass", encrypted);
            Log.d(TAG, Base64.encodeToString(vals, Base64.DEFAULT));
        } catch (Exception e) {
            Toast.makeText(this, "Exception " + e.getMessage() + " occured", Toast.LENGTH_LONG).show();
            Log.d(TAG, Log.getStackTraceString(e));
        }
    }


    private Cipher getCipher() {
        try {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) { // below android m
                return Cipher.getInstance("RSA/ECB/PKCS1Padding", "AndroidOpenSSL"); // error in android 6: InvalidKeyException: Need RSA private or public key
            } else { // android m and above
                return Cipher.getInstance("RSA/ECB/PKCS1Padding", "AndroidKeyStoreBCWorkaround"); // error in android 5: NoSuchProviderException: Provider not available: AndroidKeyStoreBCWorkaround
            }
        } catch (Exception exception) {
            throw new RuntimeException("getCipher: Failed to get an instance of Cipher", exception);
        }
    }

    public String decryptString(String alias, String key) {
        try {
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(alias, null);
            PrivateKey privateKey = privateKeyEntry.getPrivateKey();

            Cipher output = getCipher();


            output.init(Cipher.DECRYPT_MODE, privateKey);

            String cipherText = key;


            CipherInputStream cipherInputStream = new CipherInputStream(
                    new ByteArrayInputStream(Base64.decode(cipherText, Base64.DEFAULT)), output);
            ArrayList<Byte> values = new ArrayList<>();
            int nextByte;
            while ((nextByte = cipherInputStream.read()) != -1) {
                values.add((byte) nextByte);
            }

            byte[] bytes = new byte[values.size()];
            for (int i = 0; i < bytes.length; i++) {
                bytes[i] = values.get(i).byteValue();
            }

            String finalText = new String(bytes, 0, bytes.length, "UTF-8");

            Log.d(TAG, "decrypted : " + finalText);
            return finalText;
            //decryptedText.setText(finalText);

        } catch (Exception e) {
            Toast.makeText(this, "Exception " + e.getMessage() + " occured", Toast.LENGTH_LONG).show();
            Log.d(TAG, Log.getStackTraceString(e));
            return null;
        }
    }


    void setShared(String key, String value) {
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString(key, value).apply();
    }

    String getShared(String key) {
        return PreferenceManager.getDefaultSharedPreferences(mContext).getString(key, null);
    }

//    public class KeyRecyclerAdapter extends ArrayAdapter<String> {
//
//        public KeyRecyclerAdapter(Context context, int textView) {
//            super(context, textView);
//        }
//
//        @Override
//        public int getCount() {
//            return keyAliases.size();
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            View itemView = LayoutInflater.from(parent.getContext()).
//                    inflate(R.layout.list_item, parent, false);
//
//            final TextView keyAlias = (TextView) itemView.findViewById(R.id.keyAlias);
//            keyAlias.setText(keyAliases.get(position));
//            Button encryptButton = (Button) itemView.findViewById(R.id.encryptButton);
//            encryptButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    encryptString(keyAlias.getText().toString());
//                }
//            });
//            Button decryptButton = (Button) itemView.findViewById(R.id.decryptButton);
//            decryptButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    decryptString(keyAlias.getText().toString());
//                }
//            });
//            final Button deleteButton = (Button) itemView.findViewById(R.id.deleteButton);
//            deleteButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    deleteKey(keyAlias.getText().toString());
//                }
//            });
//
//            return itemView;
//        }
//
//        @Override
//        public String getItem(int position) {
//            return keyAliases.get(position);
//        }
//
//    }
}
