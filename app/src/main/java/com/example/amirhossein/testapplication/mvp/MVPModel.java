package com.example.amirhossein.testapplication.mvp;

public class MVPModel implements MVPContract.Model {


    private MVPContract.Presenter presenter;

    @Override
    public void attachPresenter(MVPContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void getName() {
        presenter.onName("Alireza");
    }


}
