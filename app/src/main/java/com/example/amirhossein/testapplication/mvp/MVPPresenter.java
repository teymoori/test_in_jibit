package com.example.amirhossein.testapplication.mvp;

public class MVPPresenter implements MVPContract.Presenter {

    MVPModel model = new MVPModel();
    private MVPContract.View view;

    @Override
    public void attachView(MVPContract.View view) {
        this.view = view;
        model.attachPresenter(this);
        model.getName();
    }

    @Override
    public void onName(String name) {
        view.showName(name);
    }
}
