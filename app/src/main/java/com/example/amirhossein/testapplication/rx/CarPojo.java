package com.example.amirhossein.testapplication.rx;

public class CarPojo {
    String name ;
    String family ;
    int id ;

    public CarPojo(String name, String family, int id) {
        this.name = name;
        this.family = family;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
