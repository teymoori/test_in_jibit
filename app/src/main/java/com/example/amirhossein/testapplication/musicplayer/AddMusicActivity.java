package com.example.amirhossein.testapplication.musicplayer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.example.amirhossein.testapplication.MyEditText;
import com.example.amirhossein.testapplication.R;
import com.example.amirhossein.testapplication.musicplayer.pojo.MusicPojo;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

@EActivity(R.layout.activity_add_music)
public class AddMusicActivity extends AppCompatActivity {


    @ViewById
    MyEditText title;

    @ViewById
    MyEditText album;

    @ViewById
    MyEditText singer;

    @ViewById
    MyEditText cover;

    @ViewById
    MyEditText url;

    @Click
    void add() {
        MusicPojo.newBuilder().album(album.text()).cover(cover.text()).singer(singer.text()).time((int) System.currentTimeMillis()).title(title.text()).url(url.text()).build().save();
        clean();
        getList();
    }

    void clean() {
        title.setText("");
        album.setText("");
        singer.setText("");
        cover.setText("");
        url.setText("");
    }

    void getList() {
        List<MusicPojo> musics = MusicPojo.listAll(MusicPojo.class);
        Toast.makeText(this, musics.get(0).getTitle() + "", Toast.LENGTH_SHORT).show();
    }

    @Click
    void goToList() {
        MusicPlayerActivity_.intent(this).start();
        //putintent
    }
}
