package com.example.amirhossein.testapplication.musicplayer;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.amirhossein.testapplication.R;
import com.example.amirhossein.testapplication.musicplayer.pojo.MusicPojo;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

@EActivity(R.layout.activity_music_player)
public class MusicPlayerActivity extends AppCompatActivity {
    Context mContext = this;
    List<MusicPojo> musics = new ArrayList<>();
    MusicsListAdapter adapter;

    @ViewById
    ListView musicsList;

    @AfterViews
    void init() {
        musics = MusicPojo.listAll(MusicPojo.class);
        if (musics.size() > 0) {
            adapter = new MusicsListAdapter(this, musics);
            musicsList.setAdapter(adapter);
        }

        musicsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String id = ((MusicPojo) adapterView.getItemAtPosition(i)).getId().toString();
                Intent musicIntent = new Intent(mContext, com.example.amirhossein.testapplication.musicplayer.MusicService.class);
                musicIntent.putExtra("music_id", id);
                startService(musicIntent);
            }
        });
    }


}
