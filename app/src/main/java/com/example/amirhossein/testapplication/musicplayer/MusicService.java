package com.example.amirhossein.testapplication.musicplayer;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.amirhossein.testapplication.R;
import com.example.amirhossein.testapplication.musicplayer.pojo.MusicPojo;

import java.util.List;

public class MusicService extends Service {

    MediaPlayer mediaPlayer;
    String TAG = "music_debug";
    NotificationCompat.Builder mBuilder;
    private NotificationManager mNotificationManager;

    @Override
    public void onCreate() {
        mediaPlayer = new MediaPlayer();
        super.onCreate();
        timer();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        String musicID = intent.getStringExtra("music_id");

        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            mediaPlayer = new MediaPlayer();
        }

        try {
            mediaPlayer.setDataSource(getMusicByID(musicID).getUrl());
            mediaPlayer.prepare();
            mediaPlayer.start();
            showNotification(getMusicByID(musicID));
        } catch (Exception e) {
            //datasource not valid
        }

        return super.onStartCommand(intent, flags, startId);
    }


    MusicPojo getMusicByID(String id) {
        return MusicPojo.findById(MusicPojo.class, Long.parseLong(id));
    }


    void timer() {
        if (mediaPlayer.isPlaying()) {
            int totalSec = mediaPlayer.getDuration() / 1000;
            int current = mediaPlayer.getCurrentPosition() / 1000;
            int percentage = (int) (current * 100.0 / totalSec + 0.5);

            mBuilder.setOngoing(true);
            mBuilder.setProgress(100, percentage, false);
            mNotificationManager.notify(0, mBuilder.build());

        } else {
            if (mNotificationManager != null)
                mNotificationManager.cancelAll();
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                timer();

            }
        }, 1000);
    }

    void showNotification(MusicPojo musicPojo) {

        mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("default",
                    "Music player",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("Music player");
            mNotificationManager.createNotificationChannel(channel);
            channel.setSound(null, null);
        }
        Log.d(TAG, "shownotif: inside show Notif");
        mBuilder = new NotificationCompat.Builder(getApplicationContext(), "default")
                .setSmallIcon(R.mipmap.ic_launcher) // notification icon
                .setContentTitle(musicPojo.getTitle()) // title for notification
                .setContentText(musicPojo.getAlbum()) // message for notification
                .setProgress(100, 0, false)
                .setColor(getResources().getColor(R.color.green))
//                    .setColorized(true)
                .setAutoCancel(true); // clear notification after click
        Intent intent = new Intent(getApplicationContext(), MusicPlayerActivity_.class);
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pi);


        //image
        Glide.with(getApplicationContext()).asBitmap().load(musicPojo.getCover()).into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap bitmap, Transition<? super Bitmap> transition) {
                if (bitmap != null) {
                    NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
                    bigPictureStyle.bigPicture(bitmap);
                    mBuilder.setStyle(bigPictureStyle);
                    mNotificationManager.notify(0, mBuilder.build());
                }
            }
        });

//        mNotificationManager.notify(0, mBuilder.build());

    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
