package com.example.amirhossein.testapplication;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.List;

public class GetInstallersActivity extends AppCompatActivity {
    String TAG = "app_2" ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_installers);
        getPackages() ;
    }

    void getPackages(){
        final PackageManager pm = getPackageManager();
//get a list of installed apps.
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        for (ApplicationInfo packageInfo : packages) {
            Log.d(TAG, packageInfo.packageName+ " : " + returnInstaller( packageInfo.packageName));
        }
    }

    String returnInstaller(String pkg) {
        try {
            return getPackageManager().getInstallerPackageName(pkg);
        } catch (Exception e) {
            return "unknown";
        }
    }

}
