package com.example.amirhossein.testapplication.rx;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.amirhossein.testapplication.BaseActivity;
import com.example.amirhossein.testapplication.R;
import com.github.nisrulz.sensey.Sensey;
import com.github.nisrulz.sensey.ShakeDetector;
import com.jakewharton.rxbinding.view.RxView;
import com.jakewharton.rxbinding.widget.RxTextView;
import com.jakewharton.rxbinding.widget.TextViewAfterTextChangeEvent;
import com.jakewharton.rxbinding.widget.TextViewTextChangeEvent;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import rx.Scheduler;
import rx.Subscriber;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;


public class RXActivity extends BaseActivity {
    String TAG = "rx_";
    ImageView capture ;
    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rx);
//        Sensey.getInstance().init(this);
        capture = findViewById( R.id.capture) ;

//        ShakeDetector.ShakeListener shakeListener=new ShakeDetector.ShakeListener() {
//            @Override public void onShakeDetected() {
//                Toast.makeText(RXActivity.this, "start", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override public void onShakeStopped() {
//                Toast.makeText(RXActivity.this, "stopped", Toast.LENGTH_SHORT).show();
//                capture.setImageBitmap(takeScreenshot());
//            }
//        };
//        Sensey.getInstance().startShakeDetection(shakeListener);


        TextView txt = findViewById(R.id.txt);


        RxTextView.textChangeEvents(txt)
                .debounce(400, TimeUnit.MILLISECONDS)
                .map(new Func1<TextViewTextChangeEvent, String>() {
                    @Override
                    public String call(TextViewTextChangeEvent text) {
                        return text.text().toString();
                    }
                })
                .filter(new Func1<String, Boolean>() {
                    @Override
                    public Boolean call(String text) {
                        return (text.length() > 2);
                    }
                }).subscribe(new Subscriber<String>() {
            @Override
            public void onNext(String s) {
                Log.d(TAG, "onNext: " + s);
            }

            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
            }


        });


        RxView.clicks(findViewById(R.id.click))
                .debounce(2000, TimeUnit.MILLISECONDS).subscribe(new Subscriber<Void>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Void aVoid) {
                Log.d(TAG, "onNext: ");
            }
        });

       Observabls.getData().subscribe(new Subscriber<String>() {
           @Override
           public void onCompleted() {

           }

           @Override
           public void onError(Throwable e) {

           }

           @Override
           public void onNext(String o) {
               Log.d(TAG, "onNext:__ " + o);
           }
       }) ;

    }



}
